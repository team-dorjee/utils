# utils

This package is a wrapper for some of the more frequent text parsing and io related
tasks for common/ngs file formats. Currently, it includes two modules, and I believe
that we would have more in the future:

* common_tasks
* ngs_file_parser

### Importing package

To import everything:
```python
from utils import *
```

To import specific function:
```python
# from common_tasks
from utils import write_to_file, compress_file
# from ngs_file_parser
from utils import from_info_string
```
