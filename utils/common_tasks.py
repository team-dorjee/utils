"""

"""
import os
import sys


def sort_by_column_index(filename):
    from operator import itemgetter
    with open(filename, 'r') as fh:
        lines = [line.split() for line in fh.read().splitlines()]
    return sorted(lines, key=itemgetter(1, 2))


def sorted_naturally(l, key):
    import re
    """ Sort the given iterable in the way that humans expect."""
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda item: [ convert(c) for c in re.split('([0-9]+)', key(item)) ]
    return sorted(l, key = alphanum_key)


def compress_file(filename):
    from subprocess import check_output, CalledProcessError
    try:
        check_output(['gzip', '-f', filename])
    except CalledProcessError as e:
        sys.exit("failed to gzip, returned code {}".format(e.returncode))
    except OSError as e:
        sys.exit("Failed to execute 'gzip': '{}'".format(str(e)))


def remove(filename):
    import errno
    try:
        os.remove(filename)
    except OSError as e:  # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise


def write_to_file(filename=None, data=None, format='txt', extension=None, mode=None,
                  source='Analysis', reference='hg19', query=None):
    """'data' has to be list of lists or list of tuples"""
    import csv
    import time

    _date = time.strftime("%Y%m%d")

    # sets the mode to 'write' if nothing is assigned
    # TODO: there has to be a better way then passing 'mode' values around
    mode = 'w' if mode is None else mode
    if filename is None:
        raise Exception("File doesn't exist.")

    delimiter = {
        'txt': ' ',
        'csv': ',',
        'tsv': '\t',
    }

    _metadata = """##fileformat=VCFv4.0
##fileDate=%s
##source=%s
##reference=%s
##INFO=<ID=END,Number=1,Type=Integer,Description=End position of the variant>
##INFO=<ID=NORM,Number=1,Type=Float,Description=Normalized value>
##FILTER=<ID=PASS,Description="Variants that passed the FILTER">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=NV,Number=1,Type=Integer,Description="Number of reads containing variant in this sample">
##FORMAT=<ID=VS,Number=1,Type=Float,Description="VS description here">
##FORMAT=<ID=NS,Number=1,Type=Float,Description="Number of Samples With Data">
##FORMAT=<ID=RAT,Number=1,Type=Float,Description="Ratio">
##ALT=<ID=DEL:GAIN:ALU,Description="Deletion, Gains and ALU element">
"""

    if data:
        filename = '.'.join((filename, extension)) if extension else filename
        with open(filename, mode) as fh:
            writer = csv.writer(fh, delimiter=delimiter[format])
            if extension == 'vcf':
                fh.write(_metadata % (_date, source, reference))

                _header = ('#CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT', '{}'.format(query))
                data.insert(0, _header)

            for row in data:
                row_stripped = map(str.strip, row) if all(isinstance(x, str) for x in row) else row
                writer.writerow(row_stripped)


def is_valid_directory(_path):
    """Checks if it's a valid directory path."""
    if os.path.isdir(_path):
        return True


def is_valid_file(_file):
    """Checks if the file name is valid."""
    _file = os.path.abspath(_file)
    if os.path.isfile(_file):
        return True


def filename_by_extension(_input, _extension):
    """Returns a list of file(s) with the matching extension"""
    _files = []
    if is_valid_directory(_input):
        if os.path.isdir(_input):
            _files.extend([
                os.path.join(_input, file) for file in os.listdir(_input) if file.endswith(_extension)
            ])
    elif is_valid_file(_input):
        _files.append(_input)

    if not _files:
        print("Input must be either a {0} file or a directory containing multiple {0} files".format(_extension))
        sys.exit(1)

    return _files


def extract_filename(path):
    return os.path.splitext(os.path.split(path)[1])


def speed_test(func):
    """Decorator that prints to STDIO the total time taken for a method execution"""
    import time
    def wrapper(*args, **kwargs):
        t1 = time.time()
        func(*args, **kwargs)
        print('"{}" took {} sec.'.format(func.__name__, time.time() - t1))


def humanize(bytes, units=('bytes','KB','MB','GB','TB', 'PB', 'EB')):
    """Returns a human readable string reprentation of bytes"""
    return str(bytes) + units[0] if bytes < 1024 else humanize(bytes>>10, units[1:])


def size_from_humanized_value(value):
    import re
    value = re.match(r'([0-9]+)([a-zA-Z]+)', value, re.I)
    size, unit = value.groups() if value else None
    return int(size), unit


def read_bam_file(bam_file, f_value=None, F_value=None):
    from subprocess import Popen, PIPE
    # samtools = '/gpfs0/home/bainbridge/software/bin/samtools'
    if f_value and F_value:
        cmd = ['samtools', 'view', '-f', str(f_value), '-F', str(F_value), bam_file]
    else:
        cmd = ['samtools', 'view', bam_file]
    p = Popen(cmd, stdout=PIPE)
    for line in p.stdout:
        yield line.decode('utf-8')