"""
VCF related parser
TODO: lookup online first - don't want to reinvent the wheel!
"""


def extract_vcf_column_info(filename, colname=None, key=None, key_separator=';', vcf_line=False):
    """given a VCF file this function will either give you a pandas dataframe or column values 
    of specified column kwarg or the column item(s) specified by the 'key' kwarg"""
    
    import re
    import pandas as pd
    
    with open(filename, 'r') as fh:
        lines = [line.split('\t') for line in fh.read().splitlines() if not line.startswith('##')]

    header = lines[0]
    data = lines[1:]

    # this shouldn't happen in a VCF, however if there are lines that starts with '#' 
    # after the header, just remove them
    data = [line for line in data if not line[0].startswith('#')]
    
    df = pd.DataFrame(data, columns=header)
    
    # if no kwargs are specified, return pandas dataframe
    if not colname and not key:
        return df
    
    if colname and not key:
        return [_data for _data in df[colname]]
    
    if colname and key:
        values = []
        for _data in df[colname]:
            # add double quotes around GENEINFO key as they should be pull together
            # _data = re.sub(r'GENEINFO=(.*)', r'GENEINFO="\1"', _data)

            # replace all ';' to '$' in GENEINFO key as they should be pull together
            geneinfo = _data.split('GENEINFO=')[1].replace(';','$')
            _data = re.sub(r'GENEINFO=(.*)', r'GENEINFO={}'.format(geneinfo), _data)
            coldata = {i[0]: i[1:] for i in [k.split('=') for k in _data.strip().split(key_separator)]}

            if key in coldata:
                values.append(coldata[key])
        
        gene_vcf_info = {}
        for i, value in enumerate(values):
            line = df.iloc[[i]].values.tolist()[0]
            for genes in [v.split(',') for v in value]:
                for gene in genes:
                    gene_vcf_info[gene] = line
                    
        if vcf_line:
            return gene_vcf_info
        else:
            return values
    
    
def from_info_string(_string=None, _key=None, split_char='='):
    d = {i[0].replace('!', ''): i[1:] for i in [r.strip().split(split_char) for r in _string.split(';')]}
    if _key in d:
        return ''.join(d[_key])
